let sampleArr = `
    [
        {
            "email":"test@example.com",
            "password":"test1234",
            "isAdmin":false
        },
        {
            "email":"user@example.com",
            "password":"password123",
            "isAdmin":true
        }
    ]
`;

console.log(sampleArr);
// .parse() : use to convert json array to JS array and save it as a variable
    let parsedSampleArr = JSON.parse(sampleArr);
    console.log(parsedSampleArr);
    console.log(parsedSampleArr.pop());

    sampleArr = JSON.stringify(parsedSampleArr);
    console.log(sampleArr);

// mini-activity
    let jsonArr = `
        [
            "pizza",
            "hamburger",
            "spaghetti",
            "shanghai",
            "hodog stick on a pineapple",
            "pancit bihon"
        ]
    `;
    console.log(jsonArr)

    let parseJsonArr = JSON.parse(jsonArr);
    console.log(parseJsonArr.pop())
    console.log(parseJsonArr)

    jsonArr = JSON.stringify(parseJsonArr);
    console.log(jsonArr);